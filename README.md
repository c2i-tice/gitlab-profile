# Commission inter IREM TICE



## Le réseau des IREM

Le réseau des Instituts de Recherche sur l’Enseignement des Mathématiques (IREM) associe des enseignants du primaire, du secondaire et du supérieur, pour mener en commun des réflexions sur l’enseignement des mathématiques et proposer ensuite des formations, des textes ou des publications aux professeurs de cette discipline.


Les IREM sont des instituts :

- de recherches centrées sur les perspectives et problématiques spécifiques qui apparaissent à tous les niveaux dans l’enseignement des mathématiques ;
- de formation des enseignants par des actions s’appuyant sur les recherches fondamentales et appliquées ;
- de production et de diffusion de supports éducatifs (articles, brochures, manuels, revues, logiciels, documents multi-médias, …).



## C2i : Commissions inter IREM

Les commissions Inter-IREM sont des groupes de travail constitués de membres de différents IREM. Certaines sont centrées sur un cycle d’études, telles la COPIRELEM et les commissions Collège ou Lycée, d’autres sur un thème, telles les commissions Épistémologie, TICE ou Statistiques et probabilités, d’autres sur un type d’activité, telle la commission Repères IREM ou Publimath.

![](logo_irem.jpg)


## C2i TICE

La Commission Inter-IREM TICE (C2i TICE) s’intéresse à tous les aspects relatifs aux TICE (Technologie de l’Information et de la Communication pour l’Enseignement) dans l’enseignement des mathématiques.

Elle a pour objectifs de : faire le point sur les différentes utilisations des TICE ; collecter, orienter, structurer et harmoniser les travaux de recherche au sein des IREM ; ouvrir de nouveaux champs de recherche concernant l’utilisation de l’outil numérique ; préparer et intervenir à des colloques et universités d’été en collaboration avec les organismes institutionnels ; suivre les évolutions techniques et réfléchir à leur intérêt pour l’enseignement.

![](logo_c2i_tice.jpg)
